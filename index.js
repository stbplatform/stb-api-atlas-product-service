'use strict';
var product = require('./lib/product');
var productService = require('./lib/productService');

module.exports = function (model) {
  product(model);
  productService(model);
};
