'use strict';
var u = require("@atdw/atdw-util").utilities;

module.exports = function resolveAccess(masterdata, productId, req) {

  var policyData = u.find(masterdata, {md: 'policies', language: 'default'});
  var policies = policyData.data.policies;

  var roles, query;
  if (req.user) {
    roles = req.user.roles;
  }

  u.each(policies.Access_All_Distribution_Listings.Roles, function (role) {
    if (roles.indexOf(role) > 0) {
      query = {id: productId};
    }
  });

  if (!query) {
    u.each(policies.Access_Selected_Listings.Roles, function (role) {
      if (roles.indexOf(role) > 0) {
        var rolesArray = role.split('_');
        if (rolesArray[1] === 'State') {
          query = {id: productId, 'physicalAddress.state': rolesArray[2]};
        } else if (rolesArray[1] === 'Cat') {
          query = {and: [{id: productId}, {or: [{category: rolesArray[2]}, {category: rolesArray[3]}]}]};
        }
      }
    });
  }
  return query;
};
