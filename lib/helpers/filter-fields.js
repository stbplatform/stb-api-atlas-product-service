'use strict';
var u = require('@atdw/atdw-util').utilities;

module.exports = function filterFields (res, fieldsToFilter) {
  var result = res;
  u.each(fieldsToFilter, function (field) {
    if(u.get(result, field.fieldName)) {
      if(field.subFieldName) {
        u.remove(result[field.fieldName], field.subFieldName, field.subFieldValue);
      } else {
        result = u.deepOmit(result, field.fieldName);
      }
    }
  });

  return result;
};