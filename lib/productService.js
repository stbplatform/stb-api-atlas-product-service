'use strict';

var Promise = require('bluebird');
var fs = require("fs");
var path = require('path');
var u = require('@atdw/atdw-util').utilities;
var Handlebars = u.Handlebars;
var resolveAccess = require('./helpers/access-control-resolver');
var xml2jsonparser = require('xml2json');
var log = require('@atdw/atdw-logging');

var xmlTemplatePromise =
  Promise.promisify(fs.readFile)(path.resolve(__dirname, '../templates/GetProductServiceXML.hbs'), {
    encoding: 'utf-8'
  })
  .then(function(source) {
    return Handlebars.compile(source);
  });
var jsonTemplatePromise =
  Promise.promisify(fs.readFile)(path.resolve(__dirname, '../templates/GetProductServiceJSON.hbs'), {
    encoding: 'utf-8'
  })
  .then(function(source) {
    return Handlebars.compile(source);
  });

module.exports = function(model) {
  model.productservice = function(key, productId, serviceId, out, mv, expired, ctx, cb) {

    var validation = Promise.try(function() {
      if (!(productId && serviceId)) {
        var err = new Error("Product Id and Service Id are mandatory.");
        err.status = err.statusCode = 400;
        return Promise.reject(err);
      }
    });
    out = out ? out.toLowerCase() : 'xml';

    if (mv) {
      mv = mv.toUpperCase();
      if (mv === 'DEFAULT') {
        mv = null;
      }
    }

    var publishedListingModel = this.app.models['publishedListing'];
    var masterDataPromise = validation.then(function() {
      return model.app.models.masterData.find();
    });

    var publishedListingPromise = masterDataPromise
      .then(function(masterdata) {
        var query = resolveAccess(masterdata, productId, ctx.req);

        if (!query) {
          var err = new Error("Unauthorized");
          err.status = err.statusCode = 401;
          return Promise.reject(err);
        }
        return query;
      }).then(function(query) {
        return publishedListingModel.findOne({
          where: query
        });
      }).then(function(publishedListing) {
        if (!publishedListing || publishedListing.status === 'INACTIVE') {
          var err = new Error("Listing not found");
          err.status = err.statusCode = 404;
          return Promise.reject(err);
        }
        else if(publishedListing.status === 'EXPIRED'){
          if(!expired || (expired.toUpperCase() !=='YES')){
            var err = new Error("Listing not found");
            err.status = err.statusCode = 404;
            return Promise.reject(err); 
          }
        }
        return publishedListing;
      });

    var productTemplatePromise = validation.then(function() {
      return Promise.try(function() {
        if (!out || out.match(/xml/i)) {
          return xmlTemplatePromise;
        } else if (out.match(/json/i)) {
          return jsonTemplatePromise;
        }
        var err = new Error("Invalid parameter: OUT with value: " + out + " - Valid types are XML or JSON");
        err.status = err.statusCode = 400;
        return Promise.reject(err);
      });
    });
    Promise.all([publishedListingPromise, masterDataPromise, productTemplatePromise]).spread(function(publishedListing, masterData, template) {
      
      var policyData = u.find(masterData, {
        md: 'policies',
        language: 'default'
      });
      var policies = policyData.data.policies;
      var userAccess = '';
      var roles;
      if (ctx.req.user) {
        roles = ctx.req.user.roles;
      }
      u.each(policies.Access_State_Specific_Tags.Roles, function(role) {
        if (roles.indexOf(role) > 0) {
          userAccess = 'stateSpecific';
        }
      });

      u.each(policies.Access_All_Tags.Roles, function(role) {
        if (roles.indexOf(role) > 0) {
          userAccess = 'adminAccess';
        }
      });

      var publishedService;
      for (var p = 0; p < publishedListing.services.length; p++) {
        if (serviceId === publishedListing.services[p].id) {
          publishedService = publishedListing.services[p];
        }
      }
      if (!publishedService) {
        var err = new Error("Service not found");
        err.status = err.statusCode = 404;
        return Promise.reject(err);
      }

      if (publishedService && publishedService.tags && userAccess === 'stateSpecific' && ctx.req.user.organisation.physicalAddress) {
        for (var i = 0; i < publishedService.tags.length; i++) {
          if (publishedService.tags[i].scope !== 'GLOBAL' && ctx.req.user.organisation.physicalAddress.state !== publishedService.tags[i].scope) {
            publishedService.tags.splice(i, 1);
            i--;
          }
        }
      } else if (publishedService && publishedService.tags && userAccess !== 'adminAccess' && (userAccess === '' || !ctx.req.user.organisation.physicalAddress)) {
        for (var i = 0; i < publishedService.tags.length; i++) {
          if (publishedService.tags[i].scope !== 'GLOBAL') {
            publishedService.tags.splice(i, 1);
            i--;
          }
        }
      } else if (publishedService && publishedService.tags && userAccess === 'adminAccess' && ctx.req.user.organisation.name === 'Tourism Australia') {
        for (var i = 0; i < publishedService.tags.length; i++) {
          if (['GLOBAL','TA'].indexOf(publishedService.tags[i].scope) < 0) {
            publishedService.tags.splice(i, 1);
            i--;
          }
        }
      }

      var organisationId;
      if (ctx.req.user) {
        organisationId = ctx.req.user.organisationId;
      }

      var protocol = (ctx.req.headers['cf-visitor'] && ctx.req.headers['cf-visitor'].indexOf('https') >= 0)?'https':'http';
      var prodoductLanguage = mv ? mv : 'en';
      var templateResponse = template({
        listing: publishedListing,
        service: publishedService,
        protocol: protocol,
        host: ctx.req.headers.host,
        productId: productId,
        organisationId: organisationId,
        prodoductLanguage: prodoductLanguage,
        md: masterData,
        mv: mv
      });

      log.trace('@atdw/atdw-api-atlas-product-service::productService.js - templateResponse: ', templateResponse);
      if (out === 'json') {
        var transformedResponse = JSON.parse(templateResponse);
        return transformedResponse;
      } else {
        return templateResponse;
      }
    }).then(function(response) {
      log.error('@atdw/atdw-api-atlas-product-service::productService.js - success ');
      if (out === 'json') {
        cb(null, 200, response);
      } else {
        cb(null, 200, {
          toXML: function() {
            return response.replace(//gi, '');
          }
        });
      }
    }).catch(function(err) {
      log.error('@atdw/atdw-api-atlas-product-service::productService.js - error occurred in calling get product ', err);
      if (out === 'json') {
        cb(null, (err.statusCode || 500), {
          error: {
            code: err.statusCode || 500,
            msg: err.message || 'Internal Server Error'
          }
        });
      } else {
        cb(null, (err.statusCode || 500), {
          toXML: function() {
            return '<?xml version="1.0" encoding="UTF-16"?> <atdw_data_results><error><msg>' + (err.message || 'Internal Server Error') + '</msg><code>' + (err.statusCode || 500) + '</code></error></atdw_data_results>'
          }
        });
      }
    });
  };

  model.remoteMethod(
    'productservice', {
      description: "Remote method for getting product service details based on productId and service Id",
      http: {
        path: '/productservice',
        verb: 'get'
      },
      accepts: [{
        arg: 'key',
        type: 'string'
      }, {
        arg: 'productid',
        type: 'string'
      }, {
        arg: 'serviceid',
        type: 'string'
      }, {
        arg: 'out',
        type: 'string'
      }, {
        arg: 'mv',
        type: 'string'
      }, {
        arg: 'expired',
        type: 'string'
      }, {
        arg: "req",
        type: "Object",
        http: {
          source: 'context'
        }
      }],
      returns: [{
        arg: 'statusCode',
        http: {
          target: 'status'
        }
      }, {
        arg: 'transformedResponse',
        type: 'object',
        root: true
      }]
    }
  );

};
