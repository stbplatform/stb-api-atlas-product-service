'use strict';

var Promise = require('bluebird');
var fs = require("fs");
var path = require('path');
var u = require('@atdw/atdw-util').utilities;
var Handlebars = u.Handlebars;
var resolveAccess = require('./helpers/access-control-resolver');
var xml2jsonparser = require('xml2json');
var filterFields = require('./helpers/filter-fields');
var log = require('@atdw/atdw-logging');

var xmlTemplatePromise =
  Promise.promisify(fs.readFile)(path.resolve(__dirname, '../templates/GetProductXML.hbs'), {
    encoding: 'utf-8'
  })
  .then(function(source) {
    return Handlebars.compile(source);
  });
var jsonTemplatePromise =
  Promise.promisify(fs.readFile)(path.resolve(__dirname, '../templates/GetProductJSON.hbs'), {
    encoding: 'utf-8'
  })
  .then(function(source) {
    return Handlebars.compile(source);
  });

module.exports = function(model) {
  function determineFieldsToFilter(ctx, masterData) {
    var fieldsToFilter = [];
    var policyData = u.find(masterData, {
      md: 'policies',
      language: 'default'
    });
    var policies = policyData.data.policies;
    var trialDistributor = u.intersection(policies.Access_Trial_Listings.Roles, ctx.req.user.roles).length > 0 ? true : false;

    if (trialDistributor) {
      var trialSubscription = u.find(ctx.req.user.organisation.subscriptions, 'status', 'TRIAL');
      var trialMetadata = trialSubscription.trial.metadata['product_res'];
      trialMetadata = Array.isArray(trialMetadata) ? trialMetadata : [trialMetadata];
      trialMetadata.forEach(function(metadata) {
        fieldsToFilter.push({
          fieldName: metadata
        });
      });
    }
    log.debug('@atdw/atdw-api-atlas-product-service::product.js - fieldsToFilter ', fieldsToFilter);
    return fieldsToFilter;
  }

  model.product = function(ctx, key, mv, productId, out, expired, cb) {
    var validation = Promise.try(function() {
      if (!productId) {
        var err = new Error("Product Id is mandatory.");
        err.status = err.statusCode = 400;
        return Promise.reject(err);
      }
    });
    out = out ? out.toLowerCase() : 'xml';

    if (mv) {
      mv = mv.toUpperCase();
      if (mv === 'DEFAULT') {
        mv = null;
      }
    }
    //Fetching the published listing Model
    var publishedListingModel = this.app.models['publishedListing'];
    var organisationModel = this.app.models['organisation'];

    var masterDataPromise = validation.then(function() {
      return model.app.models.masterData.find();
    });

    var publishedListingPromise = masterDataPromise
      .then(function(masterdata) {
        var query = resolveAccess(masterdata, productId, ctx.req);

        if (!query) {
          var err = new Error("Unauthorized Access");
          err.status = err.statusCode = 401;
          return Promise.reject(err);
        }
        return query;
      }).then(function(query) {
        return publishedListingModel.findOne({
          where: query
        })
      }).then(function(publishedListing) {
        if (!publishedListing || publishedListing.status === 'INACTIVE') {
          var err = new Error("Listing not found");
          err.status = err.statusCode = 404;
          return Promise.reject(err);
        }
        else if(publishedListing.status === 'EXPIRED'){
          if(!expired || (expired.toUpperCase() !=='YES')){
            var err = new Error("Listing not found");
            err.status = err.statusCode = 404;
            return Promise.reject(err); 
          }
        }

        return Promise.all([publishedListing, organisationModel.findById(publishedListing.owningOrganisation)]);
      }).then(function(res) {
        var publishedListing = res[0].toJSON();
        publishedListing.contributingOrganisation = res[1];
        return publishedListing;
      });

    var productTemplatePromise = validation.then(function() {
      return Promise.try(function() {
        if (!out || out.match(/xml/i)) {
          return xmlTemplatePromise;
        } else if (out.match(/json/i)) {
          return jsonTemplatePromise;
        }
        var err = new Error("Invalid parameter: OUT with value: " + out + " - Valid types are XML or JSON");
        err.status = err.statusCode = 400;
        return Promise.reject(err);
      });
    });

    Promise.all([publishedListingPromise, masterDataPromise, productTemplatePromise, mv]).spread(function(publishedListing, masterData, template, mv) {

      var policyData = u.find(masterData, {
        md: 'policies',
        language: 'default'
      });
      var policies = policyData.data.policies;
      var userAccess = '';
      var roles;
      if (ctx.req.user) {
        roles = ctx.req.user.roles;
      }
      u.each(policies.Access_State_Specific_Tags.Roles, function(role) {
        if (roles.indexOf(role) > 0) {
          userAccess = 'stateSpecific';
        }
      });

      u.each(policies.Access_All_Tags.Roles, function(role) {
        if (roles.indexOf(role) > 0) {
          userAccess = 'adminAccess';
        }
      });


      if (publishedListing.tags && userAccess === 'stateSpecific' && ctx.req.user.organisation.physicalAddress) {
        for (var i = 0; i < publishedListing.tags.length; i++) {
          if (publishedListing.tags[i].scope !== 'GLOBAL' && ctx.req.user.organisation.physicalAddress.state !== publishedListing.tags[i].scope) {
            publishedListing.tags.splice(i, 1);
            i--;
          }
        }
      } else if (publishedListing.tags && userAccess !== 'adminAccess' && (userAccess === '' || !ctx.req.user.organisation.physicalAddress)) {
        for (var i = 0; i < publishedListing.tags.length; i++) {
          if (publishedListing.tags[i].scope !== 'GLOBAL') {
            publishedListing.tags.splice(i, 1);
            i--;
          }
        }
      } else if (publishedListing.tags && userAccess === 'adminAccess' && ctx.req.user.organisation.name === 'Tourism Australia') {
        for (var i = 0; i < publishedListing.tags.length; i++) {
          if (['GLOBAL','TA'].indexOf(publishedListing.tags[i].scope) < 0) {
            publishedListing.tags.splice(i, 1);
            i--;
          }
        }
      }

      if(publishedListing.services && publishedListing.services.length) {
        publishedListing.services.forEach(function(service){
          if (service.tags && userAccess === 'stateSpecific' && ctx.req.user.organisation.physicalAddress) {
            for (var i = 0; i < service.tags.length; i++) {
              if (service.tags[i].scope !== 'GLOBAL' && ctx.req.user.organisation.physicalAddress.state !== service.tags[i].scope) {
                service.tags.splice(i, 1);
                i--;
              }
            }
          } else if (service.tags && userAccess !== 'adminAccess' && (userAccess === '' || !ctx.req.user.organisation.physicalAddress)) {
            for (var i = 0; i < service.tags.length; i++) {
              if (service.tags[i].scope !== 'GLOBAL') {
                service.tags.splice(i, 1);
                i--;
              }
            }
          } else if (service.tags && userAccess === 'adminAccess' && ctx.req.user.organisation.name === 'Tourism Australia') {
            for (var i = 0; i < service.tags.length; i++) {
              if (['GLOBAL','TA'].indexOf(service.tags[i].scope) < 0) {
                service.tags.splice(i, 1);
                i--;
              }
            }
          }
        })
      }

      var organisationId;
      if (ctx.req.user) {
        organisationId = ctx.req.user.organisationId;
      }

      var protocol = (ctx.req.headers['cf-visitor'] && ctx.req.headers['cf-visitor'].indexOf('https') >= 0)?'https':'http';

      var fieldsToFilter = determineFieldsToFilter(ctx, masterData);
      var prodoductLanguage = mv ? mv : 'en';
      var templateResponse = template({
        listing: publishedListing,
        md: masterData,
        mv: mv,
        fieldsToFilter: fieldsToFilter,
        protocol: protocol,
        host: ctx.req.headers.host,
        productId: productId,
        organisationId: organisationId,
        prodoductLanguage: prodoductLanguage
      });

      log.trace('@atdw/atdw-api-atlas-product-service::product.js - templateResponse: ', templateResponse);
      if (out === 'json') {
        var transformedResponse = JSON.parse(templateResponse);
        transformedResponse = filterFields(transformedResponse, determineFieldsToFilter(ctx, masterData));
        return transformedResponse;
      } else {
        // transformedResponse = xml2jsonparser.toJson(templateResponse, {
        //   reversible: true
        // });
        // transformedResponse = JSON.parse(transformedResponse);
        // transformedResponse = filterFields(transformedResponse, determineFieldsToFilter(ctx, masterData));
        // transformedResponse = xml2jsonparser.toXml(transformedResponse);
        // transformedResponse = "<?xml version=\"1.0\" encoding=\"UTF-16\"?>".concat(transformedResponse);        return templateResponse;
        return templateResponse;
      }
    }).then(function(response) {
      if (out === 'json') {
        cb(null, 200, response);
      } else {
        cb(null, 200, {
          toXML: function() {
            return response.replace(//gi, '');
          }
        });
      }
    }).catch(function(err) {
      log.error('@atdw/atdw-api-atlas-product-service::product.js - error occurred in calling get product ', err);
      if (out === 'json') {
        cb(null, (err.statusCode || 500), {
          error: {
            code: err.statusCode || 500,
            msg: err.message || 'Internal Server Error'
          }
        });
      } else {
        cb(null, (err.statusCode || 500), {
          toXML: function() {
            return '<?xml version="1.0" encoding="UTF-16"?> <atdw_data_results><error><msg>' + (err.message || 'Internal Server Error') + '</msg><code>' + (err.statusCode || 500) + '</code></error></atdw_data_results>'
          }
        });
      }
    });
  };

  model.remoteMethod(
    'product', {
      description: "Remote method for getting prododuct details based on productId",
      http: {
        path: '/product',
        verb: 'get'
      },
      accepts: [{
        arg: 'ctx',
        type: 'object',
        http: {
          source: 'context'
        }
      }, {
        arg: 'key',
        type: 'string'
      }, {
        arg: 'mv',
        type: 'string'
      }, {
        arg: 'productid',
        type: 'string'
      }, {
        arg: 'out',
        type: 'string'
      }, {
        arg: 'expired',
        type: 'string'
      }],
      returns: [{
        arg: 'statusCode',
        http: {
          target: 'status'
        }
      }, {
        arg: 'transformedResponse',
        type: 'object',
        root: true
      }]
    }
  );

};
