// Include gulp
var gulp = require('gulp');

// Include Our Plugins
var spawn = require('child_process').spawn;
var jshint = require('gulp-jshint');
var mocha = require('gulp-mocha');
var runSequence = require('run-sequence');
var bump = require('gulp-bump');
var gutil = require('gulp-util');
var git = require('gulp-git');
var fs = require('fs');
var argv = require('yargs').argv;
var tar = require('gulp-tar');
var gzip = require('gulp-gzip');
var rename = require('gulp-rename');
var rimraf = require('rimraf');

console.log("invoked with: ", process.argv);

function moduleInfo() {
  var info = JSON.parse(fs.readFileSync('./package.json', 'utf8'));
  var name = info.name.replace('@atdw/', '');
  var version = info.version;
  return {
    package: info,
    name: name,
    archiveName: name + '.' + version + '.tar.gz'
  };
}

// Lint Task
gulp.task('lint', function() {
    return gulp.src('js/*.js')
        .pipe(jshint())
        .pipe(jshint.reporter('default'));
});
gulp.task('mocha-test', function() {
  return gulp.src(['test/**.js'], { read: false })
    .pipe(mocha({
      reporter: 'spec',
      globals: {
        should: require('chai').should(),
        expect: require('chai').expect
      }
    }));
});
gulp.task('test', function (callback) {
  runSequence(
    'lint',
    'mocha-test',
    function (error) {
      if (error) {
        console.log(error.message);
      }
      callback(error);
    });
});

// Watch Files For Changes
gulp.task('watch', function() {
    gulp.watch('**.js', ['lint', 'mocha']);
});

gulp.task('bump-patch-version', function () {
  return gulp.src(['./package.json'])
    .pipe(bump({type: "patch"}).on('error', gutil.log))
    .pipe(gulp.dest('./'));
});
gulp.task('bump-prerelease-version', function () {
	return gulp.src(['./package.json'])
    .pipe(bump({type: "prerelease"}).on('error', gutil.log))
    .pipe(gulp.dest('./'));
});
gulp.task('commit-changes', function () {
  return gulp.src('.')
    .pipe(git.commit('[buildbox] Bumped version number'));
});
gulp.task('push-changes', function (cb) {
  git.push(argv.remote || 'origin', argv.branch || 'master', {args: '--tags'}, function(err){
    if(err) {
      return cb(err);
    }
    git.push(argv.remote || 'origin', argv.branch || 'master', cb);
  });
});
gulp.task('create-new-tag', function (cb) {
  var version = getPackageJsonVersion();
  git.tag(version, 'Created Tag for version: ' + version, function (error) {
    if (error) {
      return cb(error);
    }
    cb();
  });

  function getPackageJsonVersion () {
    //We parse the json file instead of using require because require caches multiple calls so the version number won't be updated
    return JSON.parse(fs.readFileSync('./package.json', 'utf8')).version;
  };
});

gulp.task('build:clean', function (cb) {
  return rimraf('build/**/*', { force: true }, cb);
});

gulp.task('build:package', function (cb) {
  try {
    fs.mkdirSync('build');
  } catch (err) {}

  spawn('npm', ['pack']).stdout.on('data', function(data){
    data = data.toString().trim();
    console.log(data);
    var source = fs.createReadStream(data);
    var dest = fs.createWriteStream('build/' + moduleInfo().archiveName);

    source.pipe(dest);
    source.on('end', function() { return rimraf(data, { force: true }, cb) });
    source.on('error', function(err) { cb(err) });
  });
});

gulp.task('build:publish-upload', function (done) {
  spawn('npm', ['publish', './build/' + moduleInfo().archiveName], { stdio: 'inherit' }).on('close', done);
});

gulp.task('build:publish', function (callback) {
  runSequence(
    'build:clean',
    'build:package',
    'build:publish-upload',
    function (error) {
      if (error) {
        console.log(error.message);
      }
      callback(error);
    }
  );
});

gulp.task('npm-publish', function (done) {
  spawn('npm', ['publish'], { stdio: 'inherit' }).on('close', done);
});

gulp.task('release', function (callback) {
  runSequence(
    'bump-patch-version',
    'commit-changes',
    'create-new-tag',
    'npm-publish',
    'push-changes',
    function (error) {
      if (error) {
        console.log(error.message);
      } else {
        console.log('RELEASE FINISHED SUCCESSFULLY');
      }
      callback(error);
    });
});

gulp.task('prerelease', function (callback) {
  runSequence(
    'bump-prerelease-version',
    'npm-publish',
    function (error) {
      if (error) {
        console.log(error.message);
      } else {
        console.log('PRERELEASE FINISHED SUCCESSFULLY');
      }
      callback(error);
    });
});

gulp.task('dist:clean', function (cb) {
  return rimraf('dist/**/*', { force: true }, cb);
});
gulp.task('dist:archive:tarball', function () {
  var info = moduleInfo();
  var target = gulp.src(['**','**.tar.gz','!test/**','!build/**','!dist/**']);
  return target
    .pipe(rename(function (path) {
        path.dirname = info.name + "/" + path.dirname;
      }))
    .pipe(tar(info.archiveName))
    .pipe(gzip({ append: false }))
    .pipe(gulp.dest('./dist'));
});
gulp.task('dist', function (callback) {
  runSequence(
    'dist:clean',
    'dist:archive:tarball',
    function (error) {
      if (error) {
        console.log(error.message);
      } else {
        console.log('Distribution created');
      }
      callback(error);
    }
  );
});

// Default Task
gulp.task('default', ['lint', 'mocha-test', 'watch']);
