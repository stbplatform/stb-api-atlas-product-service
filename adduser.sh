#!/usr/bin/expect -f

# CONFIGURE NPM SETTINGS
#echo "@atdw:registry=http://35.154.147.41:4873" >> .npmrc
#echo "unsafe-perm=true"
#echo "http://35.154.147.41:4873/:verd@cci@:YOUR_NPM_PASSWORD" >> .npmrc
#echo "http://35.154.147.41:4873/:stbverdaccio:YOUR_NPM_USERNAME" >> .npmrc
#echo "http://35.154.147.41:4873/:pipeline@stb.com:YOUR_NPM_EMAIL" >> .npmrc
#echo "http://35.154.147.41:4873/:always-auth=false" >> .npmrc

npm cache clean --force
export NPM_USERNAME=verdacciostb
export NPM_PASSWORD=p@ssw@rd
export NPM_EMAIL=pipeline@stb.com

npm adduser <<! $NPM_USERNAME $NPM_PASSWORD $NPM_EMAIL !

# Publish!
npm publish
